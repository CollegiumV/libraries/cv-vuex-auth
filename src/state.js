function getToken() {
  let token = localStorage.getItem('cv_auth_token')

  if (!token) {
    token = sessionStorage.getItem('cv_auth_token')
  }
  return token
}

function parseJwt(token) {
  /* https://stackoverflow.com/questions/38552003/how-to-decode-jwt-token-in-javascript */
  const base64Url = token.split('.')[1]
  const base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}

export default State = {
  token: getToken(),
  user: getToken() ? parseJwt(getToken()).uid : null,
  error: null
}
