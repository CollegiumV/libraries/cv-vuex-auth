export default Mutations = {
  setToken(state, token) {
    state.token = token
  },

  clearToken(state) {
    state.token = null
  },

  setError(state, message) {
    state.error = message
  },

  clearError(state) {
    state.error = null
  }
}
