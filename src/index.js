import State from './state'
import Actions from './actions'
import Mutations from './mutations'
import Getters from './getters'

export default CVAuth = {
  constructor() {
    this.state = State
    this.actions = Actions
    this.mutations = Mutations
    this.getters = Getters
  }
}
